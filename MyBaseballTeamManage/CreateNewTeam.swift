//
//  CreateNewTeam.swift
//  MyBaseballTeamManage
//
//  Created by 牧村岳 on 2019/10/26.
//  Copyright © 2019 gaku.maki. All rights reserved.
//

import UIKit
class CreateNewTeam: UIViewController {
    override func viewDidLoad() {
        super.viewDidLoad()
		inputPasswordText1.isSecureTextEntry = true
		inputPasswordText2.isSecureTextEntry = true
		
        // Do any additional setup after loading the view.
    }
	
	//DB 登録先のURL
	let saveToPHPURL = "http://localhost/BBapi/api/InsertTeam.php"
	
	/* チーム名のテキスト*/
	@IBOutlet weak var inputNameText: UITextField!
	
	/* 登録ボタンを押した時に実施 */
	@IBAction func RegistrerButtonClick(_ sender: Any) {
		registerMethod()
	}
	
	/* チームIDのてテキスト*/
	@IBOutlet weak var inputIDText: UITextField!
	
	/* チーム短縮名のテキスト*/
	@IBOutlet weak var inputShortenText: UITextField!
	
	@IBOutlet weak var inputPasswordText1: UITextField!
	
	@IBOutlet weak var inputPasswordText2: UITextField!
	
	
	/// <#Description#>
	func registerMethod()
	{
		var teamStruct = TeamStruct(name: inputNameText.text!, id: inputIDText.text!, shorten: inputShortenText.text!,password: inputPasswordText1.text!)
		// 文字判定
		if checkInputTextEmpty(teamStruct: teamStruct)
		{
			if(checkStringByteSize(add: teamStruct.shorten))
			{
				if(checkPassword())
				{
					teamStruct.shorten = teamStruct.shorten.uppercased()
					requestRegisterData(teamStruct: teamStruct)
				}
			}
		}
		else
		{
			print("テスト")
			showAlert(message: "入力されていない項目があります。")
		}
		
	}
	
	
	/// パスワードの確認
	///
	/// - Returns: return value description
	func checkPassword() -> Bool
	{
		let password = inputPasswordText1.text!
		if password.lengthOfBytes(using: String.Encoding.utf8) < 8
		{
			showAlert(message: "パスワードは半角8文字以内を入力してください。")
			return false
		}
		
		if password != inputPasswordText2.text
		{
			showAlert(message: "確認パスワードが異なっています。")
			return false
		}
		return true
	}
	
	/// チームデータモデル
	struct TeamStruct
	{
		let name : String!
		let id : String!
		var shorten : String!
		let password : String!
	}
	
	/// JSON結果
	struct ResultJson : Codable
	{
		let error : Bool!
		let message : String!
	}
	
	
	/// チームデータを保存する
	///
	/// - Parameter teamStruct: チームモデル
	func requestRegisterData(teamStruct : TeamStruct)
	{
		guard let url = URL(string: saveToPHPURL)
		else
		{
			return;
		}
		
		var request = URLRequest(url: url)
		request.httpMethod = "POST"
		
		let postParameters = "name=" + teamStruct.name + "&id=" + teamStruct.id + "&shorten=" + teamStruct.shorten + "&password=" + teamStruct.password
		request.httpBody = postParameters.data(using : String.Encoding.utf8)
		
		let task = URLSession.shared.dataTask(with: request,completionHandler: {
		
			data,response,error in
			if let error = error {
				print("Error  -> \(error)")
				return
			}
			
			do{
				let decoder = JSONDecoder()
				let json = try decoder.decode(ResultJson.self, from: data!)
				print("json:\(json)")
				if(!json.error)
				{
					//self.showAlert(message: "登録に成功しました。")
				}
				else
				{
					//self.showAlert(message: "登録に失敗しました。")
				}
			}catch let ex{
				//self.showAlert(message: "処理に問題が発生しました。")
				print("test")
				print (ex)
			}
		})
		task.resume()
	}

	/// チェック空テキストに入力かくにん
	///
	/// - Parameter teamStruct: <#teamStruct description#>
	/// - Returns: <#return value description#>
	func checkInputTextEmpty(teamStruct : TeamStruct ) -> Bool
	{
		var flag = true
		if teamStruct.name.isEmpty{
			flag = false
		}
		else if teamStruct.id.isEmpty
		{
			flag = false
		}
		else if teamStruct.shorten.isEmpty
		{
			flag = false
		}
		return flag
	}
	
	
	/// 入力項目が空であるか判定する
	///
	/// - Parameter add: <#add description#>
	/// - Returns: <#return value description#>
	func checkStringByteSize(add : String) -> Bool
	{
		if add.isAlphanumeric(){
			// 大文字変換
			let upAdd = add.uppercased()
			if upAdd.lengthOfBytes(using: String.Encoding.utf8) > 3
			{
				showAlert(message: "短縮名は半角３文字以内で入力してください")
				return false
			}
		}
		else{
			showAlert(message: "短縮名は半角文字で入力してください。")
			return false
		}
		return true
	}
	
	
	/// アラート形式のメッセージボックスを表示する
	/// - Parameter message: メッセージボックスに表示するメッセージ指定
	func showAlert(message : String)
	{
		let title = "入力エラー"
		let okText = "閉じる"
		
		let alert = UIAlertController(title: title, message: message, preferredStyle: UIAlertController.Style.alert)
		let defaultAction : UIAlertAction = UIAlertAction(title: okText, style: UIAlertAction.Style.default, handler: {
			// ボタンが押された時の処理を書く（クロージャ実装）
			(action: UIAlertAction!) -> Void in
			print("Cancel")
		})
	
	   alert.addAction(defaultAction)
	   present(alert, animated: true)
	}
	/*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */
	
}

extension String {
	// 半角英数字を判定
	func isAlphanumeric() -> Bool {
		return NSPredicate(format: "SELF MATCHES %@", "[a-zA-Z0-9]+").evaluate(with: self)
	}
}
