//
//  TeamMainView.swift
//  MyBaseballTeamManage
//
//  Created by 牧村岳 on 2019/11/03.
//  Copyright © 2019 gaku.maki. All rights reserved.
//

import UIKit

class TeamMainView: UIViewController,UITableViewDelegate,UITableViewDataSource {
	@IBOutlet weak var planTableView: UITableView!
	
	@IBOutlet weak var gameResultTableView: UITableView!
	
	var gamePlanData :[GamePlan] = [GamePlan]()
	var gameResultData :[GameResult] = [GameResult]()
	
	override func viewDidLoad() {
		super.viewDidLoad()
		
		// デリゲートの設定
		planTableView.delegate = self
		planTableView.dataSource = self
		planTableView.register(UINib(nibName: "PlanTableViewCell", bundle: nil), forCellReuseIdentifier: "PlanTableViewCell")
		
		
		self.gameResultTableView.delegate = self
		self.gameResultTableView.dataSource = self
		self.gameResultTableView.register(UINib(nibName: "GameResultTableViewCell", bundle: nil), forCellReuseIdentifier: "GameResultTableViewCell")
		
		self.setGamePlanData()
		self.setGameResultData()
	}
	
	func setGamePlanData()
	{
		let dateFormater = DateFormatter()
		dateFormater.locale = Locale(identifier: "ja-JP")
		dateFormater.dateFormat = "yyyy/MM/dd HH:mm:ss"
		let date = dateFormater.date(from: "2019/12/01 13:00:00") ?? Date()
		let date2 = dateFormater.date(from: "2019/12/02 15:00:00") ?? Date()
		print("test")
		print(date)
		self.gamePlanData = [GamePlan(gameDateTime: date, opponent: "SW"),GamePlan(gameDateTime: date2, opponent: "DG")]
	}
	
	func setGameResultData()
	{
		let dateFormater = DateFormatter()
		dateFormater.locale = Locale(identifier: "ja-JP")
		dateFormater.dateFormat = "yyyy/MM/dd HH:mm:ss"
		let date = dateFormater.date(from: "2019/11/01 09:30:00") ?? Date()
		
		let team1 = Team(name: "SK", score: 3)
		let team2 = Team(name: "unk", score : 2)
		
		let gameResult = GameResult(team1: team1, team2: team2, gameDate: date)
		self.gameResultData = [gameResult]
	}
	
	//
	override func didReceiveMemoryWarning() {
		super.didReceiveMemoryWarning()
		// Dispose of any resources that can be recreated.
	}
	
	/// 試合予定てテーブルに出す行数は１行
	///
	/// - Parameter tableView: <#tableView description#>
	/// - Returns: <#return value description#>
	func numberOfSections(in tableView: UITableView) -> Int {
		return 1
	}
	
	func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
		if(tableView.tag == 0)
		{
			return self.gamePlanData.count <= 3 ? self.gamePlanData.count : 3
		}
		else
		{
			return self.gameResultData.count <= 3 ? self.gameResultData.count : 3
		}
	}
	
	func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
		if (tableView.tag == 0)
		{
			let cell = tableView.dequeueReusableCell(withIdentifier: "PlanTableViewCell", for: indexPath ) as! PlanTableViewCell
			cell.setCell(gamePlan: self.gamePlanData[indexPath.row])
			return cell
		}
		else
		{
			let cell = tableView.dequeueReusableCell(withIdentifier: "GameResultTableViewCell", for: indexPath ) as! GameResultTableViewCell
			cell.setCells(gameResult: self.gameResultData[indexPath.row])
			return cell
		}
	}
}
