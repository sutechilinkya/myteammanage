//
//  PlanTableViewCell.swift
//  MyBaseballTeamManage
//
//  Created by 牧村岳 on 2019/11/04.
//  Copyright © 2019 gaku.maki. All rights reserved.
//

import UIKit
import Foundation

class PlanTableViewCell: UITableViewCell {
	
	/// 試合開始日付のラベル
	@IBOutlet weak var labelDate: UILabel!
	
	// 試合開始時間のラベル
	@IBOutlet weak var labelStartTime: UILabel!
	
	// 試合相手のラベル
	@IBOutlet weak var labelOpponent: UILabel!
	
	override func awakeFromNib() {
		super.awakeFromNib()
		// Initialization code
	}
	
	func setCell(gamePlan : GamePlan)
	{
		self.labelDate.text = gamePlan.gameDate
		self.labelStartTime.text = gamePlan.gameStartTime
		self.labelOpponent.text = gamePlan.opponent
	}
	
	override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
	}
}

class GamePlan  : NSObject{
	var gameDate : String
	var gameStartTime : String
	var opponent : String
	
	init(gameDateTime : Date,opponent : String )
	{
		let formatter = DateFormatter()
		formatter.locale = Locale(identifier: "ja_JP")
		formatter.setLocalizedDateFormatFromTemplate("yMMMdE")
		print(formatter.string(from: gameDateTime))
		self.gameDate = formatter.string(from: gameDateTime)
		formatter.setLocalizedDateFormatFromTemplate("jm")
		self.gameStartTime = formatter.string(from: gameDateTime)
		self.opponent = opponent
	}
	
}
