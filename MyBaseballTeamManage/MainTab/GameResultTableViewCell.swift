//
//  GameResultTableViewCell.swift
//  MyBaseballTeamManage
//
//  Created by 牧村岳 on 2019/11/05.
//  Copyright © 2019 gaku.maki. All rights reserved.
//

import UIKit

class GameResultTableViewCell: UITableViewCell {
	@IBOutlet weak var labelTeam1: UILabel!
	
	
	@IBOutlet weak var labelTeam1Score: UILabel!
	
	@IBOutlet weak var labelTeam2: UILabel!
	
	@IBOutlet weak var labelTeam2Score: UILabel!
	
	@IBOutlet weak var labelGameDate: UILabel!
	
	override func awakeFromNib() {
		super.awakeFromNib()
		// Initialization code
	}
	
	func setCells(gameResult : GameResult)
	{
		labelTeam1.text = gameResult.team1.name
		labelTeam1Score.text = String(gameResult.team1.score)
		labelTeam2.text = gameResult.team2.name
		labelTeam2Score.text = String(gameResult.team2.score)
		
		let formatter = DateFormatter()
		formatter.locale = Locale(identifier: "ja_JP")
		formatter.setLocalizedDateFormatFromTemplate("yMMMdE")
		labelGameDate.text = formatter.string(from: gameResult.gameDate)
	}
	
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
}

class GameResult{
	var team1 : Team
	var team2 : Team
	var gameDate : Date
	init(team1 : Team, team2 : Team,gameDate : Date)
	{
		self.team1 = team1
		self.team2 = team2
		self.gameDate = gameDate
	}
}

class Team
{
	var name : String
	var score : Int
	
	init(name : String,score  : Int)
	{
		self.name = name
		self.score = score
	}
}
