//
//  ViewController.swift
//  MyBaseballTeamManage
//
//  Created by 牧村岳 on 2019/10/24.
//  Copyright © 2019 gaku.maki. All rights reserved.
//

import UIKit

class Account
{
	var identifier : String!
	
	var password : String!
	
	init()
	{
		identifier = nil
		password = nil
	}
	
	init(identifier:String,password:String) {
		self.identifier = identifier
		self.password = password
	}
}

class ViewController: UIViewController {
	
	override func viewDidLoad() {
		super.viewDidLoad()
		// Do any additional setup after loading the view.
	}
	
	var account = Account()
	
	@IBAction func loginButtonClick(_ sender: Any) {
		self.loginMethod()
	}
	
	
	@IBOutlet weak var teamText: UITextField!
	
	@IBOutlet weak var passwordText: UITextField!
	
	@IBAction func createNewTeamButtonClick(_ sender: Any) {
		performSegue(withIdentifier: "goCreateNewTeam", sender: nil)
	}
	
	func loginMethod() {
		let text = teamText.text!
		if text.isEmpty {
			return
		}
		
		let password = passwordText.text!
		if password.isEmpty
		{
			return
		}
		let account = Account(identifier: text, password: password)
		self.login(account: account)
	}
	
	func login(account : Account) {
		//DB 登録先のURL
		let loginPHPURL = "http://localhost/BBapi/api/loginTeam.php"
		guard let url = URL(string: loginPHPURL)
			else
		{
			print("URLエラー")
			return
		}
		
		var request = URLRequest(url: url)
		request.httpMethod = "POST"
		
		let parameter = "identifier=" + account.identifier + "&password=" + account.password
		
		request.httpBody = parameter.data(using: String.Encoding.utf8)
		let task = URLSession.shared.dataTask(with: request,completionHandler: {
			data,response,error in
			if let error = error {
				print("Error  -> \(error)")
			}
			
			do{
				let decoder = JSONDecoder()
				let json = try decoder.decode(ResultJson.self, from: data!)
				print("json:\(json)")
				self.account = account
				self.loginRequestResult(result: !json.error)
			}catch let ex{
				print (ex)
				self.showAlert(message: "ログインに失敗しました。")
			}
		})
		task.resume()
	}
	
	/// JSON結果
	struct ResultJson : Codable
	{
		let error : Bool!
		let message : String!
	}
	
	func loginRequestResult(result : Bool)
	{
		if(result)
		{	
			print("segue")
			DispatchQueue.main.sync {
				performSegue(withIdentifier: "goTeamMain", sender: nil)
			}
		}
		else
		{
			print("notsegue")
			DispatchQueue.main.sync {
				self.showAlert(message: "ログインできません。")
			}
		}
	}
	
	/*
	/// Description
	override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
		if(segue.identifier == "goTeamMains")
		{
			if let nextviewController = segue.destination as? TeamMain
			{
				nextviewController.account = self.account
			}
		}
	}
*/
	
	/// アラート形式のメッセージボックスを表示する
	/// - Parameter message: メッセージボックスに表示するメッセージ指定
	func showAlert(message : String)
	{
		let title = "入力エラー"
		let okText = "閉じる"
		
		let alert = UIAlertController(title: title, message: message, preferredStyle: UIAlertController.Style.alert)
		let defaultAction : UIAlertAction = UIAlertAction(title: okText, style: UIAlertAction.Style.default, handler: {
			// ボタンが押された時の処理を書く（クロージャ実装）
			(action: UIAlertAction!) -> Void in
			print("Cancel")
		})
		
		alert.addAction(defaultAction)
		present(alert, animated: true)
	}
}


